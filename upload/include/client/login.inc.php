<?php
if(!defined('OSTCLIENTINC')) die('Access Denied');

$email=Format::input($_POST['luser']?:$_GET['e']);
$passwd=Format::input($_POST['lpasswd']?:$_GET['t']);

$content = Page::lookupByType('banner-client');

if ($content) {
    list($title, $body) = $ost->replaceTemplateVariables(
        array($content->getLocalName(), $content->getLocalBody()));
} else {
    $title = __('Sign In');
    $body = __('To better serve you, we encourage our clients to register for an account and verify the email address we have on record.');
}

?>

<div class="page-title">
    <div class="container"> <div class="row"> <div class="col-md-12">
        <h1><?php echo Format::display($title); ?></h1>
        <p><?php echo Format::display($body); ?></p>
    </div></div></div>
</div>
<div class="cover login-cover">
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2 col-sm-12">
                <div class="form-cover">
                <form action="login.php" method="post" id="clientLogin">
                        <?php csrf_token(); ?>
                    <div style="">
                        <div class="login-box">
                        <strong><?php echo Format::htmlchars($errors['login']); ?></strong>
                        <div class="form-group">
                            <input id="username" class="form-control" placeholder="<?php echo __('Email or Username'); ?>" type="text" name="luser" size="30" value="<?php echo $email; ?>" class="nowarn">
                        </div>
                        <div  class="form-group">
                            <input id="passwd" class="form-control" placeholder="<?php echo __('Password'); ?>" type="password" name="lpasswd" size="30" value="<?php echo $passwd; ?>" class="nowarn"></td>
                        </div>
                        <p>
                            <input class="btn submit" type="submit" value="<?php echo __('Sign In'); ?>">
                    <?php if ($suggest_pwreset) { ?>
                            <a style="padding-top:4px;display:inline-block;" href="pwreset.php"><?php echo __('Forgot My Password'); ?></a>
                    <?php } ?>
                        </p>
                        </div>
                        <div class="sublog">
                    <?php

                    $ext_bks = array();
                    foreach (UserAuthenticationBackend::allRegistered() as $bk)
                        if ($bk instanceof ExternalAuthentication)
                            $ext_bks[] = $bk;

                    if (count($ext_bks)) {
                        foreach ($ext_bks as $bk) { ?>
                    <div class="external-auth"><?php $bk->renderExternalLink(); ?></div>
                    
                    <?php
                        }
                    }
                    
                    if ($cfg && $cfg->isClientRegistrationEnabled()) {
                        if (count($ext_bks)) echo '<hr style="width:70%"/>'; ?>
                        <div class="btn accreg">
                            <?php echo __('Not yet registered?'); ?> <a href="<?php echo ROOT_PATH; ?>/account.php?do=create"><?php echo __('Create an account'); ?></a>
                        </div>
                    <?php } ?>
                            <div class="btn aglog">
                                <b><?php echo __("I'm an agent"); ?></b> —
                                <a href="<?php echo ROOT_PATH; ?>scp/"><?php echo __('sign in here'); ?></a>
                            </div>
                        </div>
                    </div>
                    </form>
                    
                </div>
               
        
                <br>
                <p>
                If this is your first time contacting us or you've lost the ticket number, please <a href="http://jinsonathemes.com/ost/paris/open.php"> open a new ticket </a>        </p>
            </div>
        </div>
    </div>
</div>