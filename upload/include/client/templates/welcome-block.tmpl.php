<div class="welcome-section">
    <div class="container"><div class="row">
        <div class="col-md-12">
            <div class="welcome-post">
                <?php
                if($cfg && ($page = $cfg->getLandingPage())) {
                    echo $page->getBodyWithImages();
                }else { ?>
            
                        <h1>Welcome to the Support Center</h1> <p> In order to 
                        streamline support requests and better serve you, we utilize a support 
                        ticket system. Every support request is assigned a unique ticket number 
                        which you can use to track the progress and responses online. For your 
                        reference we provide complete archives and history of all your support 
                        requests. A valid email address is required to submit a ticket. </p>   
                <?php } ?>         
            </div>
        </div>
    </div>
</div>
