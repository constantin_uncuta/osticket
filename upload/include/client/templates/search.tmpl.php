<div class="kb-home-cover">
    <div class="container">
        <div class="row">
            <div class="col-md-12">


                <p>Be sure to browse our <a href="<?php echo ROOT_PATH; ?>index.php">Frequently Asked Questions (FAQs)</a> before opening a ticket                    </p>

                <div class="kb-search-form form-inline">
                    <form method="get" action="kb/faq.php">
                    <input type="hidden" name="a" value="search">
                    <input type="text" name="q" class="search form-control" placeholder="<?php echo __('Search our knowledge base'); ?>">
                    <button type="submit" class="btn btn-default"><?php echo __('Search'); ?></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
