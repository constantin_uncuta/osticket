<div class="cta-box-cover">
    <div class="container">
        <div class="row">
                                <div class="col-md-12">
                        <div id="new_ticket" class="action-box">
                            <div class="row row-no-padding row-eq-height">
                                <div class="col-md-6 act-pic">
                                <?php
                                if($cfg && $cfg->isKnowledgebaseEnabled()){
                                    //FIXME: provide ability to feature or select random FAQs ??
                                ?>
                                <br/><br/>
                                <?php
                                $cats = Category::getFeatured();
                                if ($cats->all()) { ?>
                                <h1><?php echo __('Featured Knowledge Base Articles'); ?></h1>
                                <?php
                                }

                                    foreach ($cats as $C) { ?>
                                    <div class="featured-category front-page">
                                        <i class="icon-folder-open icon-2x"></i>
                                        <div class="category-name">
                                            <?php echo $C->getName(); ?>
                                        </div>
                                <?php foreach ($C->getTopArticles() as $F) { ?>
                                        <div class="article-headline">
                                            <div class="article-title"><a href="<?php echo ROOT_PATH;
                                                ?>kb/faq.php?id=<?php echo $F->getId(); ?>"><?php
                                                echo $F->getQuestion(); ?></a></div>
                                            <div class="article-teaser"><?php echo $F->getTeaser(); ?></div>
                                        </div>
                                <?php } ?>
                                    </div>
                                <?php
                                    }
                                }
                                ?>
                                </div>
                                <div class="col-md-6">
                                    <div class="act-text">
                                        <h3>Open a New Ticket</h3>
                                        <div>Please provide as much detail as possible so we can best assist you. To update a previously submitted ticket, please login.</div>
                                        <p>
                                            <a href="<?php echo ROOT_PATH; ?>open.php" class="green button">Open a New Ticket</a>
                                        </p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
            
            <div class="col-md-12">
                <div id="check_status" class="action-box">

                    <div class="row row-no-padding row-eq-height">
                        <div class="col-md-6">
                            <div class="act-text">
                                <h3>Check Ticket Status</h3>
                                <div>We provide archives and history of all your current and past support requests complete with responses.</div>
                                <p>
                                    <a href="<?php echo ROOT_PATH; ?>view.php" class="blue button">Check Ticket Status</a>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6 act-pic">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
